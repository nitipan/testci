import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  result: any;

  @Input() num1: any;
  @Input() num2: any;

  constructor() { }

  ngOnInit() {
  }

  calculate() {
    this.result = this.num1 + this.num2;
  }
}
